mdbackup
========

Perl scripts to fully automate backups to zfs backup hosts

mdbackup is written in perl and basically goes off and connects to a bunch of remote servers (defined in a file called mdtab) via ssh or a smb mount, rsyncs the data to a location on your local zpool and then snapshots the deepest parent zfs filesystem. it renames snaps to preserve them as monthlies or annuals and purges the remainder of snaps based on a retention policy defined in that severs configuration file, normally 21 nighties, 12 monthly and 7 annuals. It all runs from cron. It does lots of nice logging and error checking and has nice health check scripts which can be used to integrate this with nagios. it's reasonably mature and stable.

for install notes, please see the README PDF files.
