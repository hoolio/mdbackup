#!/usr/bin/bash

umount /mnt/leadbeater_s
umount /mnt/leadbeater_e

mount -F smbfs "//vicoffice;backupuser:48a2B765@leadbeater/s$" /mnt/leadbeater_s
mount -F smbfs "//vicoffice;backupuser:48a2B765@leadbeater/e$" /mnt/leadbeater_e

# ls -lha /mnt/leadbeater_e/
