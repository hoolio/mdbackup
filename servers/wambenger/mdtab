# MDTAB; The gursh configuration file.  Please read these very interesting highly critical help notes or die.
#  Grand Unified Rsync and SnapsHot; a cleverer way to copy data onto our mdbackup server, and do logging and stuff.
#
# THE DESTINATION MUST RESIDE WITHIN AN EXISTING ZFS FILESYSTEM, OR IT CAN'T BE SNAPSHOTTED.  Usually just ensure destination is under /Backups/ somewhere
#
# THE SOURCE PATH MUST EXIST, AND FOR REMOTE SERVERS MUST IT BE ACCESSABLE OVER SSH
#   If this is a samba mount, make sure it's mounted beforehand etc.  
#   If the source is a ssh path, gursh will test it has connectivity before allowing rsync to copy.  ensure that passwordless root ssh keys are configured with the remote server.
#
# THE DESTINATION PATH MUST ALREADY EXIST, OR GURSH WILL SKIP YOUR COPY AND MOVE ON TO THE NEXT.  This is unlike rsync which will create subfolders at the destination.  
#   if you need to create the destination path, you have two options.  make a new zfs filesystem or simply a new directory.  If in doubt ask Julius.
#   NEW FILESYSTEM: if the source is something you really really care about, then it should probably be snapshotted uniquely, not fossilised with unrelated data.
#                   we can separate it by making a special zfs filesystem just for that data, ala `zfs create Backups/vicoffice/onsite/Wambenger/var/<your new filesystem>`.  
#                   Note, you can't create zfs filesystems underneath directories, only undereath other zfs filesystems.  You can obviously create directories underneath filesystems.
#   NEW DIRECTORY:  else if you don't really really care that this data is handled uniquely, then just use mkdir -p to create the path.  Your data will be preserved in a more generic snapshot.
#
# If the destination path does exist, gursh will snapshot it's closest parent zfs filesystem, therefore ensuring your data remains preserved.
#
# NOTE: Currently all snapshots of filesystems are kept for the same time, regardless of anything else.  The above considerations become relevant when disk space is an issue and we need to delete stuff.
#
# You can use spaces or tabs to separate values.  Like rsync, make sure you use trailing spaces for the path names, else rsync does silly things.
#
# Iff you have a zfs filesystem blah, with zfs children called mah and gah, if you add blah to mdtab gursh will automatically snapshot mah and gah too.  that means by simply making descendent
#  zfs filesystems, you can ensure your important data gets snapshotted.  you don't need to add explicit mdtab lines for mah and gah, that's all taken care of automatically

# TESTING USE ONLY
#/usr/local/				/Backups/vicoffice/onsite/Wambenger/usr/local/
#/etc/					/Backups/vicoffice/onsite/Wambenger/etc/
#hoolio@blah:/tmp/			/tmp/
#twsadmin@wambenger:/usr/local/		/Backups/vicoffice/onsite/Wambenger/usr/local/

# PRODUCTION
/usr/local/				/Backups/vicoffice/onsite/Wambenger/usr/local/
/mnt/leadbeater_s/WD_Docs/		/Backups/vicoffice/onsite/Leadbeater/wd_docs/
/mnt/leadbeater_s/comms/		/Backups/vicoffice/onsite/Leadbeater/comms/
/mnt/leadbeater_s/thecabinet/		/Backups/vicoffice/onsite/Leadbeater/thecabinet/
/mnt/leadbeater_s/userdata/		/Backups/vicoffice/onsite/Leadbeater/userdata/
/mnt/leadbeater_s/archived/		/Backups/vicoffice/onsite/Leadbeater/archived/
/mnt/leadbeater_e/			/Backups/vicoffice/onsite/Leadbeater/e/

root@ig88:/var/				/Backups/vicoffice/onsite/IG88/var/
root@ig88:/opt/backups/                 /Backups/vicoffice/onsite/IG88/opt/backups/
root@ig88:/etc/                     /Backups/vicoffice/onsite/IG88/etc/
