#!/usr/bin/bash

umount /mnt/tarkine_quickbooksdata

mount -F smbfs -o dirperms=755 -o fileperms=755 "//natoffice;backupuser:thai5ieK@relive/rebackups$" /mnt/relive_rebackups
mount -F smbfs -o dirperms=777 -o fileperms=777 "//natoffice;backupuser:thai5ieK@relive/restore" /mnt/relive_restore

#mount -F smbfs -o dirperms=755 -o fileperms=755 "//natoffice;backupuser:thai5ieK@elgrande/refiles" /mnt/elgrande_refiles

mount -F smbfs -o dirperms=755 -o fileperms=755 "//natoffice;backupuser:thai5ieK@evergreen/livebackup" /mnt/evergreen_livebackup

mount -F smbfs -o dirperms=755 -o fileperms=755 "//natoffice;backupuser:thai5ieK@tarkine/quickbooks data" /mnt/tarkine_quickbooksdata

mount -F smbfs -o dirperms=755 -o fileperms=755 "//natoffice;backupuser:thai5ieK@wielangta/c$" /mnt/wielangta_c
mount -F smbfs -o dirperms=755 -o fileperms=755 "//natoffice;backupuser:thai5ieK@wielangta/newdata$" /mnt/wielangta_newdata
mount -F smbfs -o dirperms=755 -o fileperms=755 "//natoffice;backupuser:thai5ieK@wielangta/drimages$" /mnt/wielangta_drimages
mount -F smbfs -o dirperms=755 -o fileperms=755 "//natoffice;backupuser:thai5ieK@wielangta/isoshare$" /mnt/wielangta_isoshare 
mount -F smbfs -o dirperms=755 -o fileperms=755 "//natoffice;backupuser:thai5ieK@wielangta/refiles" /mnt/wielangta_refiles
