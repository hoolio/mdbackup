package Mdutils;

#    Copyright 2008-2012 Julius Roberts.  This file is part of mdbackup
#
#    mdbackup is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    mdbackup is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with mdbackup.  If not, see <http://www.gnu.org/licenses/>.

use 5.006001;
use strict;
use warnings;

require Exporter;

our @ISA = qw(Exporter);

# Items to export into callers namespace by default. Note: do not export
# names by default without a very good reason. Use EXPORT_OK instead.
# Do not simply export all your public functions/methods/constants.

# This allows declaration	use Mdutils ':all';
# If you do not need this, moving things directly into @EXPORT or @EXPORT_OK
# will save memory.
our %EXPORT_TAGS = ( 'all' => [ qw(sub_send_to_mdlog sub_ensure_you_are_root sub_setup_logging sub_trim sub_import_configs sub_generate_random_string
	
) ] );

our @EXPORT_OK = ( @{ $EXPORT_TAGS{'all'} } );

our @EXPORT = qw(sub_send_to_mdlog sub_ensure_you_are_root sub_setup_logging sub_trim sub_import_configs sub_generate_random_string
	
);

our $VERSION = '0.01';

# Perl trim function to remove whitespace from the start and end of the string
sub sub_trim($)
{
	my $string = shift;
	$string =~ s/^\s+//;
	$string =~ s/\s+$//;
	return $string;
}

sub sub_import_configs() {
	my $hostname = `hostname`; chomp($hostname);
	my $config_path = "/usr/local/mdbackup/servers/$hostname/config"; 
	
	if (not -e $config_path) {
		print "ERROR!! config path $config_path not found, aborting!!\n";
		exit;
	} else { 
		my($cfg,%Config);
		Config::Simple->import_from($config_path, \%Config);
		$cfg = new Config::Simple($config_path);
		
		my $minimum_config_file_version = 17;
		
		my $config_file_version = $cfg->param('config_file_version');
		if ($config_file_version < $minimum_config_file_version) {
			print "ERROR!! config_file_version = $config_file_version, but minimum_config_file_version = $minimum_config_file_version, aborting!!\n";
			print "Manually diff and update $config_path against a version from ./servers/*/config\n";
			
			sub_send_to_mdlog("ERROR!! config_file_version = $config_file_version, but minimum_config_file_version = $minimum_config_file_version, aborting!!");
			sub_send_to_mdlog("Manually diff and update $config_path against a version from ./servers/*/config");
			exit;
		}
		
		return $cfg;
	}
}

sub sub_setup_logging () {
	#print "Setting up logging infrastructure\n";
	use Config::Simple;
	my $hostname = `hostname`; chomp($hostname);
	my $config_path = "/usr/local/mdbackup/servers/$hostname/config"; my($cfg,%Config);
	Config::Simple->import_from($config_path, \%Config);
	$cfg = new Config::Simple($config_path);

	my $log_root = $cfg->param('log_root');	
	my $today_symlink = $cfg->param('today_symlink');	
	my $rsync_error_file = $cfg->param('rsync_error_file');	
	my $gursh_error_file = $cfg->param('gursh_error_file');	
		
	my $starttime=`date +%Y.%m.%d_%H%M`; chomp($starttime);
	my $startdate=`date +%Y.%m.%d`; chomp($startdate);
	my $logpath="$log_root/$startdate";
	system("sudo mkdir -p $logpath");
	
	# nagios checks $today_symlink every X mins.  it will error if it can't find $rsync_error_file or $operations_error_file
	#  we create that race condition when we delete $today_symlink.  so that's why we touch the logfiles so that they're at least
	#  empty (vs non-existant) when it checks.
	system("sudo rm $today_symlink");	
	system("sudo ln -s $log_root/$startdate $today_symlink");	
	system("sudo touch $rsync_error_file");
	system("sudo touch $gursh_error_file");

	return($starttime);
}

sub sub_send_to_mdlog ($) {
	# this script will always try to write to $operations_log and assumes it exists
	# it assumes one line of data, it probably should accept an array and then work through the elements

	use Config::Simple;
	my $hostname = `hostname`; chomp($hostname);
	my $config_path = "/usr/local/mdbackup/servers/$hostname/config"; my($cfg,%Config);
	Config::Simple->import_from($config_path, \%Config);
	$cfg = new Config::Simple($config_path);

	my $operations_log = $cfg->param('operations_log');
	
	use POSIX qw/strftime/;
	#local(my $incoming_data=$_[0]);
	my $incoming_data=$_[0]; chomp($incoming_data); 
	my $the_name_of_this_script = $0;	
		
	if ($incoming_data eq '') { 
		# then there's no point logging a null value 
	} else {
		sub_ensure_you_are_root(); open MDLOG, ">>$operations_log" or die $!;	
		print MDLOG strftime('%Y/%m/%d %H:%M:%S',localtime)." ".$the_name_of_this_script." - ".$incoming_data."\n";
		close MDLOG; 	
	}
}

sub sub_ensure_you_are_root() {
	# ensure this is being run as root
	my $whoami = getpwuid($>);
	if ($whoami ne "root") {
       print "This command needs to be run as root (use sudo)\n";
       exit;
	}
}

sub sub_generate_random_string($) {
	my $length_of_randomstring=shift;# the length of 
			 # the random string to generate

	my @chars=('a'..'z','A'..'Z','0'..'9','_');
	my $random_string;
	foreach (1..$length_of_randomstring) 
	{
		# rand @chars will generate a random 
		# number between 0 and scalar @chars
		$random_string.=$chars[rand @chars];
	}
	return $random_string;
}

# Preloaded methods go here.

1;
__END__
# Below is stub documentation for your module. You'd better edit it!

=head1 NAME

Mdutils - Perl extension for doing TWS stuff to zfs pools/filesystems etc

=head1 SYNOPSIS

  use Mdutils;
  sub_send_to_mdlog("your text here"); # will log this txt to the md operations log for today.
  sub_ensure_you_are_root(); # will exit the currently running script if you're not root.

=head1 DESCRIPTION

  use Mdutils;
  sub_send_to_mdlog("your text here"); # will log this txt to the md operations log for today.
  sub_ensure_you_are_root(); # will exit the currently running script if you're not root.

=head2 EXPORT

None by default.



=head1 SEE ALSO


=head1 AUTHOR

Julius Roberts, E<lt>julius.roberts@wilderness.org.auE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2011 by Julius Roberts

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.10.0 or,
at your option, any later version of Perl 5 you may have available.


=cut
