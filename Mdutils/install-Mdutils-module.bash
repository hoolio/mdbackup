#!/usr/bin/bash
cd /usr/local/mdbackup/Mdutils

echo "Running sudo perl Makefile.PL"
sudo perl Makefile.PL
echo

echo "Running sudo make"
sudo make
echo

echo "Running sudo make test"
sudo make test
echo

echo "Running sudo make install"
sudo make install
