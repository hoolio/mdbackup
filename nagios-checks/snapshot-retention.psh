#!/usr/bin/perl -w
#
#    Copyright 2008-2012 Julius Roberts.  This file is part of mdbackup
#
#    mdbackup is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    mdbackup is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with mdbackup.  If not, see <http://www.gnu.org/licenses/>.
#
# PARSE AND PRINT SNAPSHOT RETENTION, BUT DON'T ALERT (AT THIS STAGE)
#
# We have lots of snapshots on these machines, but no easy way of working out exactly what we have.
# Output like this is useless unless you're a computer.  
# $ zfs list -t snapshot -Ho name | grep -v rpool | head -4
# Backups/vicoffice/onsite/Leadbeater/archived@2011.12.09_2100_m
# Backups/vicoffice/onsite/Leadbeater/archived@2012.03.05_2100_a
# Backups/vicoffice/onsite/Leadbeater/archived@2012.03.04_2100
# Backups/vicoffice/onsite/Leadbeater/archived@gursh_2012.03.06_1031
# ..Which is good, because we have computers.  Bring it!
#
# HOW WE DO IT:
# x) parse the machine to get a list of filesystems we care about.  ie; the ones which have snapshots.
#    1) fill a large array with the names of all the snapshots, excluding rpool.
#    2) take each array element and remove the text after the @ symbol (yeilding filesystem names only), push it to a new array.
#    3) remove duplicates from the new array, yielding an array of the filesystems we care about. yay.
# x) count the number of filesystems we care about, $NFWCA :)
# x) create the 3 aggregates; total number of nightly, monthly & annual snapshots $N, $M & $A
# x) then using those aggregates divided by $NFWCA, create the 3 averages; average nightly, monthly, annual snapshots $AN, $AM & $AA.
# x) try and get tricky with some modulus figures.. ;)
# x) variously print the outputs etc
# x) exit the script with the OK nagios state; we're not alerting, Right now that's not happening because the thresholds are too intangible.

use 5.010;
use strict;
my $argument0 = "NULL"; if (@ARGV) { $argument0 = $ARGV[0]; }

# X) PARSE THE MACHINE TO GET A LIST OF FILESYSTEMS WE CARE ABOUT.  THEY ARE THOSE WHICH HAVE SNAPSHOTS
# fill a large array with the names of all the snapshots
my (@raw_filesystem_list, @filesystems_we_care_about, @filesystems_we_care_about_sorted);
my @raw_snapshot_list=`/sbin/zfs list -t snapshot -Ho name | grep -v rpool`;

# chuck away anything after the @ and put the rest into an array
foreach (@raw_snapshot_list) { 
	chomp($_);	my ($cleansed_element,$blah)=split("@",$_);	push (@raw_filesystem_list, $cleansed_element);
}
# remove the duplicates from the array.  This uses hashes, i have no idea how hashes work.
my %hash   = map { $_ => 1 } @raw_filesystem_list; @filesystems_we_care_about = keys %hash;

# X) SORT AND COUNT THE NUMBER OF FILESYSTEMS WE CARE ABOUT, $NFWCA :)
@filesystems_we_care_about_sorted = sort(@filesystems_we_care_about);
my $NFWCA=@filesystems_we_care_about_sorted; 

# X) CREATE THE 3 AGGREGATES; TOTAL NUMBER OF NIGHTLY, MONTHLY & ANNUAL SNAPSHOTS $N, $M & $A	
my $N =0; my $M = 0; my $A = 0;
foreach (@raw_snapshot_list) { 
	given ($_) {
		when ($_ =~ m/_m/) { $M++; }
		when ($_ =~ m/_a/) { $A++; }
		default { $N++; }
	}
}

# X) THEN USING THOSE AGGREGATES DIVIDED BY $NFWCA, CREATE THE 3 AVERAGES; AVERAGE NIGHTLY, MONTHLY, ANNUAL SNAPSHOTS $AN, $AM & $AA.
my $AN = $N/$NFWCA;
my $AM = $M/$NFWCA;
my $AA = $A/$NFWCA;

# X) TRY AND GET TRICKY WITH SOME MODULUS FIGURES.. ;)
#  the logic being that in a perfect world where there were no extra snapshots (eg created in testing) and no missing snapshots (deleted by an admin or other)
#  the number of snapshots should divide evenly into $NFWCA.  ie if the modulus > 0 then (for some reason, not always bad) we have a assymetrical ratio of
#  snapshots to filesystems.  mod > 0 is a rough indicator that all is not well, particularly when it happens yet nothing has otherwise changed.
my $MN = $N%$NFWCA;
my $MM = $M%$NFWCA;
my $MA = $A%$NFWCA;

# X) PRINT THE STUFF THUSLY; $AN NIGHTS, $AM MONTHS, $AA ANNUAL ETC 
if ( $argument0 eq "/verbose" ) {
	foreach (@filesystems_we_care_about_sorted) { 
		my $current_filesystem = $_;
		my $snapshot_counter = 0;	
		foreach (@raw_snapshot_list) {
			if ($_ =~ m/$current_filesystem/) { $snapshot_counter++; }
		}
		print "$current_filesystem ($snapshot_counter snaps)\n";
	} 
	print "\n";
}

if ($MN + $MM + $MA > 0) {
	if ($MN ne 0) { print "WARNING: nightly snaps ($N) don't divide cleanly into the number of filesystems ($NFWCA).\n"; }
	if ($MM ne 0) { print "WARNING: monthly snaps ($M) don't divide cleanly into the number of filesystems ($NFWCA).\n"; }
	if ($MA ne 0) { print "WARNING: annual snaps ($A) don't divide cleanly into the number of filesystems ($NFWCA).\n"; }
	exit 1;
} else {
	if ($argument0 eq '/verbose') { print "OK: Found $N nightly, $M monthly & $A annual snaps on $NFWCA filesystems.\n"; }
	print "OK: Found $AN nightly, $AM monthly and $AA annual snaps (averaged) across $NFWCA filesystems.\n";
	exit 0;
}

